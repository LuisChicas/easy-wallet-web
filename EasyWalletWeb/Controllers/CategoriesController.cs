﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EasyWalletWeb.Infrastructure;
using EasyWalletWeb.Models;
using EasyWalletWeb.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace EasyWalletWeb.Controllers
{    
    public class CategoriesController : Controller
    {
        private readonly DatabaseContext _context;
        private readonly IStringLocalizer<CategoriesController> _localizer;

        public CategoriesController(DatabaseContext context, IStringLocalizer<CategoriesController> localizer)
        {
            _context = context;
            _localizer = localizer;
        }

        public IActionResult Index()
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var categories = _context.Categories.Include(c => c.Tags)
                .Where(c => c.UserId == int.Parse(userId) && c.DeletedAt == null)
                .OrderByDescending(c => c.CreatedAt)
                .ToList();

            foreach (var c in categories)
            {
                c.Tags = c.Tags.Where(t => t.DeletedAt == null && !t.IsAutoGenerated).ToList();
            }

            return View(new CategoriesIndex { Categories = categories });
        }

        public IActionResult New() => View("Form", new CategoriesForm { IsNew = true });        

        [HttpPost]
        public IActionResult New(CategoriesForm form)
        {
            int userId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
            var userCategories = _context.Categories.Where(c => c.UserId == userId && c.DeletedAt == null).ToArray();

            var duplicatedCategory = userCategories.FirstOrDefault(c => c.Name == form.Name);
            if (duplicatedCategory != null)
            {
                ModelState.AddModelError("Name", _localizer["NameAlreadyExists"]);
                form.IsNew = true;
                return View("Form", form);
            }

            var userTags = _context.Tags.Where(t => userCategories.Any(c => c.Id == t.CategoryId)).ToArray();

            var duplicatedTag = userTags.FirstOrDefault(t => 
                t.DeletedAt == null &&
                !t.IsAutoGenerated &&
                form.Tags.Any(tags => tags.Name == t.Name));

            if (duplicatedTag != null)
            {
                ModelState.AddModelError(
                    "Tags", 
                    string.Format(
                        "{0}{1}{2}", 
                        _localizer["KeywordAlreadyExists1"], 
                        duplicatedTag.Name, 
                        _localizer["KeywordAlreadyExists2"])
                );

                form.IsNew = true;
                return View("Form", form);
            }

            var category = new Category();
            category.UserId = userId;
            category.Name = form.Name;
            category.CategoryTypeId = Constants.ExpensesCategoryTypeID;
            category.CreatedAt = DateTime.UtcNow;

            _context.Categories.Add(category);
            _context.SaveChanges();

            Tag tag;
            int categoryId = _context.Categories.First(c => 
                c.UserId == userId && 
                c.Name == category.Name && 
                c.DeletedAt == null).Id;

            foreach (var t in form.Tags)
            {
                if (!string.IsNullOrEmpty(t.Name))
                {
                    var autogeneratedTag = userTags.FirstOrDefault(at =>
                        at.Name == t.Name &&
                        at.DeletedAt == null &&
                        at.IsAutoGenerated);

                    if (autogeneratedTag != null)
                    {
                        autogeneratedTag.DeletedAt = DateTime.UtcNow;
                    }

                    tag = new Tag();
                    tag.CategoryId = categoryId;
                    tag.Name = t.Name;
                    tag.CreatedAt = DateTime.UtcNow;
                    tag.IsAutoGenerated = false;

                    _context.Tags.Add(tag);
                }
            }

            _context.SaveChanges();

            return RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            var category = _context.Categories.Include(c => c.Tags).FirstOrDefault(c => c.Id == id);
            if (category == null || category.DeletedAt != null)
            {
                return NotFound();
            }

            return View("Form", new CategoriesForm
            {
                Id = category.Id,
                Name = category.Name,
                Tags = category.Tags.Where(t => t.DeletedAt == null && !t.IsAutoGenerated).ToList(),
                IsNew = false
            });
        }

        [HttpPost]
        public IActionResult Edit(CategoriesForm form)
        {
            int id = form.Id;
            int userId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));

            var category = _context.Categories.FirstOrDefault(c => c.Id == id);
            if (category == null || category.DeletedAt != null)
            {
                return NotFound();
            }

            var duplicatedCategory = _context.Categories.FirstOrDefault(c => 
                c.Id != id && 
                c.UserId == userId && 
                c.Name == form.Name && 
                c.DeletedAt == null);

            if (duplicatedCategory != null)
            {
                ModelState.AddModelError("Name", _localizer["NameAlreadyExists"]);
                return View("Form", form);
            }

            var userCategories = _context.Categories.Where(c => c.UserId == userId && c.DeletedAt == null).ToArray();
            var userTags = _context.Tags.Where(t => userCategories.Any(c => c.Id == t.CategoryId) && t.DeletedAt == null).ToArray();

            var duplicatedTag = userTags.FirstOrDefault(t => 
                t.CategoryId != id &&
                !t.IsAutoGenerated &&
                form.Tags.Any(tags => tags.Name == t.Name));

            if (duplicatedTag != null)
            {
                ModelState.AddModelError(
                    "Tags",
                    string.Format(
                        "{0}{1}{2}",
                        _localizer["KeywordAlreadyExists1"],
                        duplicatedTag.Name,
                        _localizer["KeywordAlreadyExists2"])
                );
                return View("Form", form);
            }

            category.Name = form.Name;

            Tag tag;
            foreach (var t in form.Tags)
            {
                if (!string.IsNullOrEmpty(t.Name) && !userTags.Any(existingTag => existingTag.Name == t.Name && !existingTag.IsAutoGenerated))
                {
                    var autogeneratedTag = userTags.FirstOrDefault(at =>
                        at.Name == t.Name &&
                        at.IsAutoGenerated);

                    if (autogeneratedTag != null)
                    {
                        autogeneratedTag.DeletedAt = DateTime.UtcNow;
                    }

                    tag = new Tag();
                    tag.CategoryId = id;
                    tag.Name = t.Name;
                    tag.CreatedAt = DateTime.UtcNow;
                    tag.IsAutoGenerated = false;
                    _context.Tags.Add(tag);
                }
            }

            foreach (var t in category.Tags)
            {
                if (!form.Tags.Any(formTag => formTag.Name == t.Name))
                {
                    t.DeletedAt = DateTime.UtcNow;
                    _context.Tags.Update(t);
                }
            }

            _context.Update(category);
            _context.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            var category = _context.Categories.FirstOrDefault(c => c.Id == id);
            if (category == null || category.DeletedAt != null)
            {
                return NotFound();
            }

            category.DeletedAt = DateTime.UtcNow;
            _context.Categories.Update(category);
            _context.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}