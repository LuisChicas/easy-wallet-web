﻿using EasyWallet.Data.Entities;

namespace EasyWallet.Data.Abstractions
{
    public interface ICategoryRepository : IRepository<CategoryData>
    {
    }
}
