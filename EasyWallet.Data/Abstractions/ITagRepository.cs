﻿using EasyWallet.Data.Entities;

namespace EasyWallet.Data.Abstractions
{
    public interface ITagRepository : IRepository<TagData>
    {
    }
}
